# nrf24l01 native library для STM32

Нативная библитека для работы с трансивером NRF2401 совместно с контроллерами STM32F4xx и др. серий.

---

Подробнее и все туториалы от создателя здесь: https://github.com/MYaqoobEmbedded/STM32-Tutorials

Зафиксированная версия: [#7a6067a](https://github.com/volodink/nrf24l01_lib/commit/7a6067a79d29fd48c1bbdd3633d4bef0b03c4bc7)

--

Property of: https://github.com/MYaqoobEmbedded/STM32-Tutorials  