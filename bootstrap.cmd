echo "Downloading libraries ..."

rem Go for Arduino libraries download.
git clone --branch v1.3.12 https://github.com/volodink/RF24.git libs/stm32_arduino/RF24
git clone --branch v1.3.12 https://github.com/volodink/RF24.git libs/avr_arduino/RF24

rem Go for native libraries download.
git clone --branch v0.1 https://github.com/volodink/nrf24l01_lib libs/stm32_native/nrf24l01_lib

echo "Done!"
